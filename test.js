const obj = {
    foo: 'bar',
    hallo: 'welt'
}

const {foo, hallo} = obj

//const foo = obj.foo
//const hallo = obj.hallo


function createObject() {
    return {
        firstName: 'Max',
        lastName: 'Muster'
    }
}

const {firstName} = createObject()

const obj2 = createObject()
const firstName = obj2.firstName


const person = {
    userId: 4,
    firstName: 'Herbert',
    lastName: 'Trinkdas'
}

function updatePerson({userId, ...personData}) {
    console.log(userId) // 4
    console.log(personData) // {firstName: '', lastName: ''}
}
updatePerson(person)






const array = ['Max', 5, 7, 9, 4, 6]

function doSomething([firstName, ...numbers]) {

}
doSomething(array)