import {defineStore} from "pinia";
import {ref} from "vue";
import axios from "axios";

const USERS_API_URL = 'https://cgrszcqmhd.user-management.asw.rest/api/users/';
export const useUserStore = defineStore('user-store', () => {
    const users = ref([])

    async function loadUsers() {
        const usersResponse = await axios.get(USERS_API_URL)
        users.value = usersResponse.data
    }

    async function saveUser(userData) {
        const userResponse = await axios.post(USERS_API_URL, userData)
        users.value.push(userResponse.data)
    }

    async function editUser(userId, userData) {
        const userResponse = await axios.put(USERS_API_URL + userId, userData)
        const existingUserIndex = users.value.findIndex(user => user.userId === userId)
        if(existingUserIndex >= 0) {
            users.value.splice(existingUserIndex, 1, userResponse.data)
        } else {
            users.value.push(userResponse.data)
        }
    }

    async function deleteUser(userId) {
        await axios.delete(USERS_API_URL + userId)
        const existingUserIndex = users.value.findIndex(user => user.userId === userId)
        if(existingUserIndex >= 0) {
            users.value.splice(existingUserIndex, 1)
        }
    }

    return {
        users,
        saveUser,
        loadUsers,
        deleteUser,
        editUser
    }
})